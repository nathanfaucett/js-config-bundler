var tape = require("tape"),
    fs = require("fs"),
    gulp = require("gulp"),
    through = require("through2"),
    fileUtils = require("@nathanfaucett/file_utils"),
    path = require("path"),
    configBundler = require("..");

function bundle(out, flatten, minify, callback) {
    var build = path.join(__dirname, out);

    if (fs.existsSync(build)) {
        fileUtils.removeSync(build);
    }

    gulp
        .src([path.join(__dirname, "config", path.join("*.js"))])
        .pipe(
            configBundler({
                options: {
                    port: 8080
                }
            })
        )
        .pipe(gulp.dest(build))
        .pipe(
            through.obj(
                function(chunk, enc, callback) {
                    callback(undefined, chunk);
                },
                function() {
                    callback();
                }
            )
        );
}

tape("config bundler flatten and minify", function(assert) {
    bundle("build", true, true, function() {
        assert.equals(
            fs.existsSync(path.join(__dirname, "build", "config.json")),
            true,
            "config.json should exists"
        );
        assert.deepEquals(
            JSON.parse(
                fs
                    .readFileSync(path.join(__dirname, "build", "config.json"))
                    .toString()
            ),
            {
                url: "http://dev.example.com:8080",
                origUrl: "http://example.com:8080",
                dev: true,
                stay: "Stay"
            }
        );
        assert.end();
    });
});
