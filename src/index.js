var path = require("path"),
    through = require("through2"),
    File = require("vinyl"),
    extend = require("@nathanfaucett/extend"),
    emptyFunction = require("@nathanfaucett/empty_function");

var returnsArgument = emptyFunction.thatReturnsArgument(),
    returnsObject = emptyFunction.thatReturns({});

module.exports = configBundler;

function configBundler(options) {
    var env = process.env.NODE_ENV || "development",
        reEnv = new RegExp("\\b" + env + "\\b"),
        getDefaults = returnsArgument,
        getEnvironments = returnsObject,
        reName,
        reNameWithExt;

    options = options || {};
    options.options = options.options || options.defaults || {};
    options.fileName = path.basename(options.fileName || "config", ".js");

    reName = new RegExp("\\b" + options.fileName + "\\b");
    reNameWithExt = new RegExp("\\b" + options.fileName + "\\.js\\b");

    return through.obj(
        function(file, enc, callback) {
            var basename = path.basename(file.path);

            if (
                reName.test(basename) &&
                reEnv.test(basename) &&
                !reNameWithExt.test(basename)
            ) {
                if (getEnvironments === returnsObject) {
                    getEnvironments = require(file.path);
                }
            } else if (reNameWithExt.test(basename)) {
                if (getDefaults === returnsArgument) {
                    getDefaults = require(file.path);
                }
            }

            callback();
        },
        function(callback) {
            var config = getDefaults(options.options),
                environments = getEnvironments(options.options, config),
                mergedConfig = extend({}, config, environments);

            this.push(
                new File({
                    path: options.fileName + ".json",
                    contents: new Buffer(JSON.stringify(mergedConfig, null, 2))
                })
            );

            callback();
        }
    );
}
